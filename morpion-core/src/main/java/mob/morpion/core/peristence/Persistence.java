package mob.morpion.core.peristence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class Persistence {

	private static ObjectMapper mapper = new ObjectMapper().setVisibility(PropertyAccessor.ALL, Visibility.NONE)
			.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

	private Persistence() {
	}

	/**
	 * Execute une fonction sur un objet en assurant sa persistence
	 * 
	 * @param function la fonction a execut�
	 * @param le       type de l'objet
	 * @return le r�sultat
	 */
	public static <O, T> T apply(Function<O, T> function, Class<O> clazz) {
		try {
			Path path = Paths.get(System.getProperty("java.io.tmpdir") + "/save.json");
			O game = load(path, clazz);
			T result = function.apply(game);
			save(path, game);
			return result;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static <O> void save(Path path, O object) throws IOException {
		mapper.writeValue(path.toFile(), object);
	}

	private static <O> O load(Path path, Class<O> clazz) throws IOException, ReflectiveOperationException {
		O object;
		if (path.toFile().exists()) {
			object = mapper.readValue(Files.readAllBytes(path), clazz);
		} else {
			object = clazz.getDeclaredConstructor().newInstance();
		}
		return object;
	}
}
