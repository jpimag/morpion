package mob.morpion.core;

import java.util.List;

import mob.morpion.core.game.Game;
import mob.morpion.core.game.Player;
import mob.morpion.core.game.State;
import mob.morpion.core.peristence.Persistence;

/**
 * Service externe pour jouer une partie de morpion
 */
public class Morpion {

	public List<Integer> player1Positions() {
		return Persistence.apply(game -> game.positions(Player.X), Game.class);
	}

	public List<Integer> player2Positions() {
		return Persistence.apply(game -> game.positions(Player.O), Game.class);
	}

	public void play(int position) {
		Persistence.apply(game -> {
			game.play(position);
			return null;
		}, Game.class);
	}

	public boolean hasPlayer1Win() {
		return Persistence.apply(game -> game.state() == State.XWIN, Game.class);
	}

	public boolean hasPlayer2Win() {
		return Persistence.apply(game -> game.state() == State.OWIN, Game.class);
	}

	public boolean isDraw() {
		return Persistence.apply(game -> game.state() == State.DRAW, Game.class);
	}

	public boolean isPlayer1Pending() {
		return Persistence.apply(game -> game.state() == State.XPENDING, Game.class);
	}

	public boolean isPlayer2Pending() {
		return Persistence.apply(game -> game.state() == State.OPENDING, Game.class);
	}

	public void newGame() {
		Persistence.apply(game -> {
			game.reset();
			return null;
		}, Game.class);
	}

}
