package mob.morpion.core.game;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Une grille de morpion
 */
public class Board {

	private List<Player> elements;

	public Board() {
		elements = new ArrayList<>();
		positions().forEach(i -> elements.add(null));
	}

	/**
	 * Un joueur prend une case
	 */
	public void set(int position, Player player) {
		if (elements.get(indexOf(position)) != null) {
			throw new IllegalArgumentException("Position already occupated");
		}
		elements.set(indexOf(position), player);
	}

	/**
	 * @return si un joueur tient une case
	 */
	public boolean is(int position, Player player) {
		return elements.get(indexOf(position)) == player;
	}

	/**
	 * 
	 * @return les positions d'un joueur
	 */
	public static Stream<Integer> positions() {
		return IntStream.rangeClosed(1, 9).boxed();
	}

	private int indexOf(int position) {
		if (position < 1 || position > 9) {
			throw new IllegalArgumentException("Illegal position");
		}
		return position - 1;
	}
}
