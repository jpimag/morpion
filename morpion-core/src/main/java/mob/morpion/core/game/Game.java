package mob.morpion.core.game;

import static java.util.Arrays.asList;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Une partie de morpion
 */
public class Game {

	private static final List<List<Integer>> WIN_POSITIONS = asList(asList(1, 2, 3), asList(4, 5, 6), asList(7, 8, 9),
			asList(1, 4, 7), asList(2, 5, 8), asList(3, 6, 9), asList(1, 5, 9), asList(3, 5, 7));

	private Board board;

	public Game() {
		reset();
	}

	/**
	 * (r�)initialise le plateau
	 */
	public void reset() {
		board = new Board();
	}

	/**
	 * @return la liste des positions d'un joueur
	 */
	public List<Integer> positions(Player player) {
		return Board.positions().filter(i -> board.is(i, player)).collect(Collectors.toList());
	}

	/**
	 * Joue le prochain coup
	 */
	public void play(int position) {
		board.set(position, state() == State.XPENDING ? Player.X : Player.O);
	}

	/**
	 * @return l'�tat du jeu
	 */
	public State state() {
		if (win(Player.O)) {
			return State.OWIN;
		}
		if (win(Player.X)) {
			return State.XWIN;
		}
		if (Board.positions().noneMatch(i -> board.is(i, null))) {
			return State.DRAW;
		}
		if (positions(Player.X).size() > positions(Player.O).size()) {
			return State.OPENDING;
		}
		return State.XPENDING;

	}

	private boolean win(Player player) {
		for (List<Integer> positions : WIN_POSITIONS) {
			if (positions.stream().allMatch(i -> board.is(i, player))) {
				return true;
			}
		}
		return false;

	}

}
