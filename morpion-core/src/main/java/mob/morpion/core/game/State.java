package mob.morpion.core.game;

/**
 * L'�tat du plateau
 */
public enum State {
	XPENDING, OPENDING, DRAW, XWIN, OWIN;
}