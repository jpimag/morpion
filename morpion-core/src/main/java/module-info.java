module morpion.core {
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.annotation;
	requires com.fasterxml.jackson.core;

	opens mob.morpion.core.game to com.fasterxml.jackson.databind;

	exports mob.morpion.core;
}