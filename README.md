# morpion

A morpion game implementation illustrating java 9 modules usage.

Branches :

- step1 : simple implementation (without third party) library without use of modules.
- step1-withmodules : same that step1 with java modules.
- step2 :  full implementation (jackson library dependency for persitence) without use of modules.
- step2-withmodules : same that step2 with java modules.


