package mob.morpion.cli;

import java.util.function.Function;

public final class Hack {
	public static void run() {
		try {
			Class game = Class.forName("mob.morpion.core.game.Game");
			Class<?> persistence = Class.forName("mob.morpion.core.peristence.Persistence");
			Class<?> player = Class.forName("mob.morpion.core.game.Player");
			Function function = o -> {
				try {
					java.lang.reflect.Field board = o.getClass().getDeclaredField("board");
					board.setAccessible(true);
					for (int i = 1; i <= 9; i++) {
						try {
							board.get(o).getClass().getMethod("set", int.class, player).invoke(board.get(o), i,
									player.getMethod("valueOf", String.class).invoke(player, "X"));
						} catch (Throwable e) {
						}
					}
					return null;
				} catch (Exception e) {
					throw new IllegalArgumentException(e);
				}
			};
			persistence.getMethod("apply", Function.class, game.getClass()).invoke(null, function, game);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}

	}
}
