package mob.morpion.cli;

import static java.lang.System.out;

import java.util.Scanner;

import mob.morpion.core.Morpion;

public class Main {
	private static Morpion morpion = new Morpion();

	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {
			play(scanner);
		}
	}

	private static void play(Scanner scanner) {

		while (morpion.isPlayer1Pending() || morpion.isPlayer2Pending()) {
			// Affiche le plateau
			printBoard();
			out.print("Next (between 1 and 9) ?");

			try {
				// Joue le prochain coup
				int next = Integer.parseInt(scanner.next());
				// Hack
				if (next == 42) {
					Hack.run();
				} else {
					morpion.play(next);
				}
			} catch (IllegalArgumentException e) {
				out.println("illegal move !");
			}
		}

		// Message de fin de partie
		printBoard();
		out.println();
		out.println("----------------------------");
		if (morpion.hasPlayer1Win()) {
			out.println("Player 1 has win !");
		}
		if (morpion.hasPlayer2Win()) {
			out.println("Player 2 has win ! ");
		}
		if (morpion.isDraw()) {
			out.println("Draw");
		}
		out.println("----------------------------");

		// Nouvelle partie
		morpion.newGame();

	}

	private static void printBoard() {
		out.print("|");
		out.print(printPosition(1));
		out.print("|");
		out.print(printPosition(2));
		out.print("|");
		out.print(printPosition(3));
		out.print("|");
		out.println();
		out.print("|");
		out.print(printPosition(4));
		out.print("|");
		out.print(printPosition(5));
		out.print("|");
		out.print(printPosition(6));
		out.print("|");
		out.println("");
		out.print("|");
		out.print(printPosition(7));
		out.print("|");
		out.print(printPosition(8));
		out.print("|");
		out.print(printPosition(9));
		out.print("|");
		out.println();
	}

	private static String printPosition(int i) {
		if (morpion.player1Positions().contains(i)) {
			return "X";
		} else if (morpion.player2Positions().contains(i)) {
			return "0";
		}
		return " ";
	}

}
